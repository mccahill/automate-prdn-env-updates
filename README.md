# Singularity Containers
This project is used to test the use of Singularity containers on SSI managed servers. Following is a description of the def files I used to create the Singularity containers and their use.


## research_rstudio.def
This definition is created from a Docker image, rocker/rstudio, based on Debian 8.7 that contains R and RStudio-Server. I then added a version of Anaconda Python based on Python 3.5. To this were added the Python modules:
	* scikit-learn
	* theano
	* tensorflow
	* caffe
	* jupyter
Additional R modules were added as well:
	* devtools
	* rstudio/tensorflow

I created the container with 8Gb (currently 1.1 Gb still free).
**sudo singularity create --size 8192 /srv/apps/rstudio.img**

### Container use:
**singularity run /srv/apps/rstudio.img** will return the name and version number of the Anaconda Python and R modules that are installed in the image (which should help with documenting reproducibility).

Your home directory is automatically passed into the container. So if you **singularity shell /srv/apps/rstudio.img** you will see your home when you enter **ls**. I created a /Work directory in the image and there was an existing /mnt in case you want to bind additional directories to the container. For example you have a CIFS share mounted at /mnt on the host you can pass that in with a bind option to singularity, **singularity shell -B /mnt:/Work /srv/apps/rstudio.img**.

To run Pthon or R you can shell in or exec (e.g. **singularity exec /srv/apps/rstudio.img python**)

To run Juypter Notebook you can run **singularity exec -B /mnt:/Work -B /run/user:/run/user /srv/apps/rstudio.img jupyter notebook --ip='*' --no-browser**, I'm think of adding an alias to the environment file that sets run_juypter_notebook="jupyter notebook --ip='*' --no-browser", but I'm not sure it would save that much typing.

To run Rstudio-Server you can run **singularity exec -B /mnt:/Work -B /run/user:/run/user /srv/apps/rstudio.img /usr/lib/rstudio-server/bin/rserver**.

## research_centos.def
This definition is created from a pull of CentOS 7 RPM's from centos.org. I have it installing the developer tools, R, RStudio-Server, Anaconda Python, and modules for R and Python.

I created the image with 8 Gb and it's currently using 7.5 Gb.
**sudo singularity create --size 8192 /srv/apps/research.img**

**singularity run /srv/apps/rstudio.img** will return the name and version number of the Anaconda Python and R modules that are installed in the image.

### Container use:

To run Pthon or R you can shell in or exec (e.g. **singularity exec /srv/apps/research.img python**)

To run Juypter Notebook you can run **singularity exec -B /run/user:/run/user /srv/apps/rstudio.img jupyter notebook --ip='*' --no-browser**.

To run Rstudio-Server you can run **singularity exec -B /run/user:/run/user /srv/apps/rstudio.img /usr/lib/rstudio-server/bin/rserver**.

